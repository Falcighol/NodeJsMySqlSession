'use strict'
const express = require('express');
const session = require('express-session');
const MySQLStore = require('express-mysql-session')(session);
const cookieParser = require('cookie-parser');
const path = require('path');
const bodyParser = require('body-parser');

const routes = require('./routes');

const options = {
	host: 'localhost',
	port: 3306,
	user: 'root',
	password: '',
	database: 'sessiontest'
};

const app = express();

const sessionStore = new MySQLStore(options);

app.use(session({
	key: 'sessionID',
	secret: 'secretFlyingCat',
	store: sessionStore,
	clearExpired: true,
	checkExpirationInterval: 60000,
	expiration: 3600000,
	resave: false,
	saveUninitialized: false
}));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(cookieParser());

app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.get('/', routes.getIndex);

app.get('/login', (req, res) => {
	if (req.session.isLoggedIn) {
		res.redirect('/');
	} else {
		res.render('login');
	}
});

app.post('/logout', (req, res) => {
	req.session.destroy();
	res.redirect('/login');
});

app.post('/login', (req, res) => {
	if (req.body.password === "12345") {
		req.session.isLoggedIn = true;
		req.session.userName = req.body.userName;
		req.session.password = req.body.password;
		req.session.save();
		res.redirect('/');
	} else {
		res.redirect('/login');
	}
});

const port = process.env.PORT || 3000;

app.listen(port, () => {
	console.log(`Listening on port ${port}...`);
});